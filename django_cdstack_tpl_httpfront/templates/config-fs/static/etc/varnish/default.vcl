vcl 4.0;

# List of upstream proxies we trust to set X-Forwarded-For correctly.
acl upstream_proxy {
    "localhost";
    "127.0.0.1";
    "::1";
}

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "{{ http_upstream_server }}";
    .port = "{{ http_upstream_port }}";
    .max_connections = 300;

    .probe = {
        # We prefer to only do a HEAD /
        .request =
            "HEAD / HTTP/1.1"
            "Host: localhost"
            "Connection: close"
            "User-Agent: Varnish Health Probe";

        # check the health of each backend every 5 seconds
        .interval  = 5s;
        # timing out after 1 second.
        .timeout   = 1s;
        # If 3 out of the last 5 polls succeeded the backend is considered healthy, otherwise it will be marked as sick
        .window    = 5;
        .threshold = 3;
    }

    # TODO: How long to wait before we receive a first byte from our backend?
    #.first_byte_timeout     = 300s;
    # TODO: How long to wait between bytes received from our backend?
    #.between_bytes_timeout  = 2s;
    # How long to wait for a backend connection?
    .connect_timeout        = 5s;
}

sub vcl_recv {
    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.

    # Remove any Google Analytics based cookies
    set req.http.Cookie = regsuball(req.http.Cookie, "__utm.=[^;]+(; )?", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "_ga=[^;]+(; )?", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "_gat=[^;]+(; )?", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "utmctr=[^;]+(; )?", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "utmcmd.=[^;]+(; )?", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "utmccn.=[^;]+(; )?", "");

    # Remove DoubleClick offensive cookies
    set req.http.Cookie = regsuball(req.http.Cookie, "__gads=[^;]+(; )?", "");
    
    # Remove the Quant Capital cookies (added by some plugin, all __qca)
    set req.http.Cookie = regsuball(req.http.Cookie, "__qc.=[^;]+(; )?", "");
    
    # Remove the AddThis cookies
    set req.http.Cookie = regsuball(req.http.Cookie, "__atuv.=[^;]+(; )?", "");
    
    # Remove Wordpress cookies
    set req.http.cookie = regsuball(req.http.cookie, "wp-settings-\d+=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "wp-settings-time-\d+=[^;]+(; )?", "");
    
    # Remove Complianz – GDPR/CCPA Cookie Consent cookies
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_banner-status=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_consented_services=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_functional=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_marketing=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_policy_id=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_preferences=[^;]+(; )?", "");
    set req.http.cookie = regsuball(req.http.cookie, "cmplz_statistics=[^;]+(; )?", "");

    # Are there cookies left with only spaces or that are empty?
    if (req.http.cookie ~ "^\s*$") {
      unset req.http.cookie;
    }
    
    # Send Surrogate-Capability headers to announce ESI support to backend
    set req.http.Surrogate-Capability = "key=ESI/1.0";
    
    # Set the X-Forwarded-For header so the backend can see the original
    # IP address. If one is already set by an upstream proxy, we'll just re-use that.
    if (client.ip ~ upstream_proxy && req.http.X-Forwarded-For) {
        set req.http.X-Forwarded-For = req.http.X-Forwarded-For;
    } else {
        set req.http.X-Forwarded-For = regsub(client.ip, ":.*", "");
    }
    
    if (req.http.Upgrade ~ "(?i)websocket") {
        return (pipe);
    }
    
    if (req.http.Authorization || req.method == "POST") {
        # Not cacheable by default
        return (pass);
    }
    
    if (req.url ~ "/feed") {
        return (pass);
    }
    
    if (req.url ~ "wp-admin|wp-login") {
        return (pass);
    }
    
    # Joomla - don't cache
    if (req.url ~ "administrator") {
        return (pass);
    }
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.
    set beresp.ttl = 5m;
    set beresp.grace = 6h;

    # Pause ESI request and remove Surrogate-Control header
    if (beresp.http.Surrogate-Control ~ "ESI/1.0") {
        unset beresp.http.Surrogate-Control;
        set beresp.do_esi = true;
    }

    return (deliver);
}

sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.
    #
    ## By removing X-Powered-By the varnish signature is removed
    unset resp.http.X-Powered-By;
    if (obj.hits > 0) {
            set resp.http.X-Cache = "HIT";
            set resp.http.X-Cache-Hits = obj.hits;
    } else {
            set resp.http.X-Cache = "MISS";
    }
    return(deliver);
}

sub vcl_pipe {
    if (req.http.upgrade) {
        set bereq.http.upgrade = req.http.upgrade;
    }
}

sub vcl_hash {
    # URL and hostname/IP are the default components of the vcl_hash
    # implementation. We add more below.
    hash_data(req.url);
    
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }

    # Include the X-Forward-Proto header, since we want to treat HTTPS
    # requests differently, and make sure this header is always passed
    # properly to the backend server.
    if (req.http.X-Forwarded-Proto) {
        hash_data(req.http.X-Forwarded-Proto);
    }

    return (lookup);
}
